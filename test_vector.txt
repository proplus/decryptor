set of parameters to test with
------------------------------

password: 123456
public_key: 9f3b005c7a54a6856c1f926d69662787330f76b3ab15de67271a0529ac52e976
plaintext file (b64): SGVsbG8sIFdvcmxkIQo=
encrypted file (b64): 7v/MAAAAASbeGc86UERnqyflj9qYhgwAnBOEhSU6VSyQoX2qv886Oh5cTIiYeQMA9W+bQY63Jz0QSg5p0eohORVZN9Zl

locked_secretbox: e774eacdba850429a6fbdf6c4bfe84f4d2b25d4d0ee82b2ec7b42461da8c556b129621f97f7122a7794bc60a265a17c0
nonce: 3d90fbbfd1e9dbbaa099b954d384f041d9004a88633128a6
salt: 312342381e75915d513a470b63ef1c9c
opslimit: 4
memlimit: 33554432
