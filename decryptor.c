/* In the Name of Allah */

/* This software decrypts a file encrypted by the TREES plugin for
 * the dovecot IMAP server and prints the decrypted content on stdout.
 * The TREES plugin is available here:
 * https://0xacab.org/riseuplabs/trees/
 * 
 * For decryption, the IMAP password and the crypto parameters used
 * during encryption need to be known.
 */

/* needs libsodium-dev 1.0.14 (or later)
 * compile with:
 * gcc -Wall -o decryptor -lsodium decryptor.c
 */

/* standard libraries */
#include <stdio.h> // for perror()
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h> // for strchr()
#include <assert.h>
#include <errno.h>
#include <ctype.h>

/* unix libraries */
#include <fcntl.h> // for open()
#include <unistd.h> // for read() / write()
#include <getopt.h> // argument parsing
#include <termios.h> // disable echo for password input

/* external libraries */
#include <sodium.h> // crypto library

/* isascii compat */
#if defined(_XOPEN_SOURCE) || defined(_DEFAULT_SOURCE) || defined(_SVID_SOURCE)
# define HAVE_ISASCII 1
#else
int isascii(int c) { return (c >= 0 && c <= 127) ? 0 : 1; }
#endif

/* fixed crypto parameters definitions */

#define SALT_BYTES crypto_pwhash_SALTBYTES
// NONCE_SIZE is the same as NONCE_BYTES
// used in some implementations and 
#define NONCE_BYTES crypto_secretbox_NONCEBYTES
// KEY_BYTES is the same as DIGEST_BYTES and KEY_SIZE
// used in some implementations and 
#define KEY_BYTES crypto_secretbox_KEYBYTES
#define SECRETBOX_BYTES (crypto_secretbox_MACBYTES+crypto_box_SECRETKEYBYTES)

// from TREES source
#define CHUNK_SIZE 8192
#define ENCRYPTED_CHUNK_SIZE (crypto_box_SEALBYTES + CHUNK_SIZE)

// select argon2i as default bc it was the only one in old libsodium versions
#define PWHASH_ALGO_DEFAULT crypto_pwhash_ALG_ARGON2I13

// define a maximum size for the user-input password buffer
#define PW_MAXLEN 256

// compile-time check for PW_MAXLEN boundaries given by libsodium
#if (PW_MAXLEN > crypto_pwhash_PASSWD_MAX || PW_MAXLEN < crypto_pwhash_PASSWD_MIN)
# error "PW_MAXLEN is not inside libsodium boundaries"
#endif

// TREES encrypted files start with this 3-byte magic number
#define FILE_MAGIC "\xee\xff\xcc"
// version value
#define VERSION_LE "\x00\x00\x00\x01"


/* helper functions */

// function to output simple usage help
void
usage(const char * const progname)
{
    fprintf(stderr, "Usage: %s --salt salt_hex --nonce nonce_hex "
                    "--secretbox secretbox_hex --opslimit <number> "
                    "--memlimit <number> [--argon2i | --argon2id] "
                    "file_to_be_decrypted\n", progname);
    fputs("'_hex' indicates a hexadecimal encoded value "
          "(no spaces or colons)\n"
          "Default pwhash is argon2i if no flag is given\n", stderr);
}

// helper function that sets the correspnding variable from an option
bool
parse_argval(int option_char, char *optarg, unsigned long long *opslimit,
             unsigned long long *memlimit, unsigned char *salt, unsigned char *nonce,
             unsigned char *secretbox)
{
    char *endptr = NULL; // for strtoull
    int rv;
    size_t hex_output_len;
    size_t optarg_len;
    assert(NULL != optarg);
    optarg_len = strlen(optarg);
    errno = 0; // reset errno just in case it was nonzero somehow
    switch(option_char)
    {
        // parse numerical value from string for opslimit/memlimit
        case 'o':
            *opslimit = strtoull(optarg, &endptr, 10);
            assert(endptr >= optarg);
            return errno != ERANGE && optarg_len == endptr - optarg;
        case 'm':
            *memlimit = strtoull(optarg, &endptr, 10);
            assert(endptr >= optarg);
            return errno != ERANGE && optarg_len == endptr - optarg;
        // parse binary value from hexstring for salt/nonce/secretbox
        case 's':
            rv = sodium_hex2bin(salt, SALT_BYTES, optarg, optarg_len,
                                   NULL, &hex_output_len, NULL);
            return 0 == rv && hex_output_len == SALT_BYTES;
        case 'n':
            rv = sodium_hex2bin(nonce, NONCE_BYTES, optarg, optarg_len,
                                   NULL, &hex_output_len, NULL);
            return 0 == rv && hex_output_len == NONCE_BYTES;
        case 'b':
            rv = sodium_hex2bin(secretbox, SECRETBOX_BYTES,
                                   optarg, optarg_len,
                                   NULL, &hex_output_len, NULL);
            return 0 == rv && hex_output_len == SECRETBOX_BYTES;
    }
    // something went wrong, this should never be reached
    return false;
}

int
main(int argc, char **argv)
{
    // --- 01 --- variables for libsodium decryption
    
    // 5 commandline parameters
    unsigned long long opslimit;
    unsigned long long memlimit;
    unsigned char salt[SALT_BYTES];
    unsigned char nonce[NONCE_BYTES];
    unsigned char secretbox[SECRETBOX_BYTES];
    //derived symmetric key to open the secretbox
    unsigned char symmetric_key[KEY_BYTES]; // CRITICAL DATA
    
    // return value for various libsodium function calls
    int sodium_rv = -1;
    
    // pub/privkey given in encrypted secretbox
    unsigned char private_key[crypto_box_SECRETKEYBYTES]; // CRITICAL DATA
    unsigned char public_key[crypto_box_PUBLICKEYBYTES];
    
    
    // --- 02 --- variables for option parsing
    
    int long_index = 0;
    int option_char = '\0';
    bool parsearg_rv = true;
    size_t args_set = 0;
    // stores which key derivation function to use:
    // either argon2i or argon2id. -1 = use default
    int pwhash_algo = -1;
    
    // getopts string, colon = option has required argument
    const char * const short_option_str = "s:n:b:o:m:";
    // long version of the short option letters
    // plus optional flag to explicitly select pwhash algorithm
    const struct option long_options[] =
    {
        {"salt",      required_argument, NULL, 's'},
        {"nonce",     required_argument, NULL, 'n'},
        {"secretbox", required_argument, NULL, 'b'},
        {"opslimit",  required_argument, NULL, 'o'},
        {"memlimit",  required_argument, NULL, 'm'},
        {"argon2i",   no_argument, &pwhash_algo, crypto_pwhash_ALG_ARGON2I13 },
        {"argon2id",  no_argument, &pwhash_algo, crypto_pwhash_ALG_ARGON2ID13},
        {NULL, 0, NULL, 0}
    };
    // highest index in long_options array
    const size_t options_index_max = 6;
    // number of required options (all except the flag)
    const size_t required_options_count = 5;
    
    
    // --- 03 --- variables for file io
    
    // file descriptor for open()/read()
    int fd = 0;
    // file needs to start with 3-byte magic
    uint8_t magic[3];
    // return value for read() calls 
    ssize_t rv = 0;
    // number of bytes to be written
    size_t wcnt = 0;
    // TREES storage file format version number
    uint8_t version[4]; // given in file

    
    // --- 04 --- variables for password entry over stdin
    
    // to temporarily modify terminal (disable echo for pw)
    struct termios oflags, nflags;
    // buffer into which we read the pw from terminal
    char password[PW_MAXLEN]; // CRITICAL DATA
    
    
    // --- 05 --- variables for file content decryption
    
    // decryption happens in chunks
    unsigned char encrypted_chunk[ENCRYPTED_CHUNK_SIZE];
    unsigned char decrypted_chunk[CHUNK_SIZE]; // CRITICAL DATA
    
    
    // --- 06 --- control flow variable
    
    bool success = false;
    
    
    // --- 07 --- initialize libsodium

    if(sodium_init() == -1)
    {
        fputs("could not initialize libsodium\n", stderr);
        return 1;
    }
    
    
    // --- 08 --- parse arguments
    
    while(parsearg_rv)
    {
        long_index = -1;
        option_char = getopt_long(argc, argv, short_option_str,
                                  long_options, &long_index);
        // last option reached
        if(-1 == option_char) break;
        // unknown option
        if('?' == option_char) return 1; // getopt_long prints warning already
        // 2nd half checks for -1 != long_index too
        assert(0 != option_char || (size_t)long_index <= options_index_max);
        // long-only option which set a flag -> do nothing more
        if(0 == option_char && long_options[long_index].flag != NULL) continue;
        // at this point the option must be one of the given chars
        assert(0 != isalpha(option_char) && 0 != isascii(option_char));
        assert(NULL != strchr(short_option_str, option_char));
        // set the correct value from the argument
        parsearg_rv = parse_argval(option_char, optarg, &opslimit, &memlimit,
                                   salt, nonce, secretbox);
        if(parsearg_rv) ++args_set;
    }
    
    // something went wrong parsing an option
    if(!parsearg_rv)
    {
        // if long_index is set, it was a long option
        if(-1 != long_index)
        {
            fprintf(stderr, "invalid argument for --%s\n",
                    long_options[long_index].name);
        } else {
            // option char is verified by two assert()s in above loop
            fprintf(stderr,"invalid argument for -%c\n", option_char);
        }
        // show help and exit with failure status
        usage(argv[0]);
        return 1;
    }
    
    
    // --- 09 --- verify arguments
    
    // can only be checked after parsing as single-char form could reduce
    // number of args by omitting space between option char and value
    
    // if an optional flag argument was counted, total number of args is +1
    if((args_set != required_options_count && pwhash_algo == -1) ||
       (args_set != required_options_count + 1 && pwhash_algo != -1) ||
    // this last part checks for the positional filename argument
       !(optind < argc))
    {
        fputs("not enough parameters\n", stderr);
        usage(argv[0]);
        return 1;
    }
    
    // currently, only one file is supported
    if(optind+1 != argc)
    {
        fputs("currently, only one file is supported\n", stderr);
        return 1;
    }

    // on some systems, unsigned long long values might be bigger than size_t
    // as the standard only requires a minimum of 65535 for SIZE_MAX
    if(memlimit > SIZE_MAX)
    {
        fputs("given memlimit is bigger than maximum supported by this system.\n", stderr);
        return 1;
    }
    
    // check for libsodium memlimit and opslimit constraints
    if(memlimit < crypto_pwhash_MEMLIMIT_MIN ||
       memlimit > crypto_pwhash_MEMLIMIT_MAX)
    {
        // libsodium fails to explicitly define a type for the two
        // constants, therefore %u and %lu, to avoid compiler warnings
        fprintf(stderr, "memlimit needs to be between %u and %lu "
                        "as required by libsodium.\n",
                crypto_pwhash_MEMLIMIT_MIN, crypto_pwhash_MEMLIMIT_MAX);
        return 1;
    }
    
    if(opslimit < crypto_pwhash_OPSLIMIT_MIN ||
       opslimit > crypto_pwhash_OPSLIMIT_MAX)
    {
        fprintf(stderr, "opslimit needs to be between %u and %u "
                        "as required by libsodium.\n",
                crypto_pwhash_OPSLIMIT_MIN, crypto_pwhash_OPSLIMIT_MAX);
        return 1;
    }
    
    // set default value for pwhash_algo if not set by flag
    if(pwhash_algo == -1) pwhash_algo = PWHASH_ALGO_DEFAULT;
    
    
    // --- 10 --- open file and test for correct file format
    
    // try to open file
    fd = open(argv[optind], O_RDONLY);
    if(-1 == fd)
    {
        perror("open()");
        fprintf(stderr, "could not open file %s for read\n", argv[optind]);
        return 1;
    }
    
    // try to read magic number from file
    rv = read(fd, magic, 3);
    if(-1 == rv)
    {
        perror("read()");
        fputs("could not read from file\n", stderr);
        return 1;
    }
    if(3 != rv)
    {
        fputs("could not read 3 bytes, file too small?\n", stderr);
        return 1;
    }
    if(0 != memcmp(FILE_MAGIC, magic, 3))
    {
        fputs("file does not start with correct magic number, corrupt?\n", stderr);
        return 1;
    }
    
    // try to read version number from file
    rv = read(fd, version, 4);
    if(-1 == rv)
    {
        perror("read()");
        fputs("could not read from file\n", stderr);
        return 1;
    }
    if(4 != rv)
    {
        fputs("could not read 7 bytes, file too small?\n", stderr);
        return 1;
    }
    if(0 != memcmp(VERSION_LE, version, 4))
    {
        fputs("file has unsupported TREES version\n", stderr);
        return 1;
    }
    
    
    // --- 11 --- get pw from stdin
    
    // disable echo
    tcgetattr(fileno(stdin), &oflags);
    nflags = oflags;
    nflags.c_lflag &= ~ECHO;
    nflags.c_lflag |= ECHONL;

    if (tcsetattr(fileno(stdin), TCSANOW, &nflags) != 0)
    {
        perror("tcsetattr");
        fputs("could not set terminal mode\n", stderr);
        return 1;
    }

    // try to get password from stdin
    fputs("password: ", stderr);
    fgets(password, PW_MAXLEN, stdin);
    if(strlen(password) >= (PW_MAXLEN - 1))
    {
        // -2 because trailing newline and trailing zero byte
        fprintf(stderr,
                "only passwords of max. %d characters are supported\n",
                (PW_MAXLEN - 2));
        goto pwread;
    }
    
    // check for pw size limits set by libsodium
    if(strlen(password) < crypto_pwhash_PASSWD_MIN ||
       strlen(password) > crypto_pwhash_PASSWD_MAX)
    {
        fprintf(stderr, "libsodium requires a password to be at least %d "
                        "and at most %d characters long\n",
                crypto_pwhash_PASSWD_MIN, crypto_pwhash_PASSWD_MAX);
        goto pwread;
    }
    
    // strip trailing newline
    if(password[strlen(password) - 1] == '\n')
    {
        password[strlen(password) - 1] = '\0';
    }

    // restore terminal flags
    if (tcsetattr(fileno(stdin), TCSANOW, &oflags) != 0)
    {
        perror("tcsetattr");
        fputs("could not set terminal mode\n", stderr);
        goto pwread;
    }
    
    
    // --- 12 --- derive fixed-size symmetric key from password
    
    // key derivation function call
    sodium_rv = crypto_pwhash(symmetric_key, KEY_BYTES,
                              password, strlen(password),
                              salt, opslimit, memlimit,
                              pwhash_algo);
    
    // do not exit with error after clearing pw memory if we reach this point
    success = true;

pwread:
    // clear mem for password
    sodium_memzero(password, sizeof(password));
    // exit with error if we reached this point by earlier error condition
    if(!success) return 1;
    // reset flow control switch
    success = false;
    
    if(0 != sodium_rv)
    {
        fputs("key derivation using crypto_pwhash failed. "
              "This usually happens due to lack of available memory.\n", stderr);
        goto pwhashed;
    }
    
    
    // --- 13 --- decrypt keypair from secretbox using symmetric key
    
    // symmetric decryption call
    sodium_rv = crypto_secretbox_open_easy(private_key,
                               secretbox, SECRETBOX_BYTES,
                               nonce, symmetric_key);
    
    if(0 != sodium_rv)
    {
        fputs("secretbox decryption failed. wrong password?\n", stderr);
        goto privkeydecrypted;
    }
    
    // instead of asking for pubkey, we just calculate it from privkey
    sodium_rv = crypto_scalarmult_base(public_key, private_key);

    if(0 != sodium_rv)
    {
        fputs("deriving public key from private key failed. wtf? "
              "there is no documentation on why this might happen.\n", stderr);
        goto privkeydecrypted;
    }
    
    
    // --- 14 --- actual file content decryption and output
    
    do
    {
        // read encrypted chunk from file
        rv = read(fd, encrypted_chunk, ENCRYPTED_CHUNK_SIZE);
        if(-1 == rv)
        {
            perror("read()");
            fputs("could not read from file\n", stderr);
            goto privkeydecrypted;
        }
        assert(rv <= ENCRYPTED_CHUNK_SIZE);
        // if no bytes are read, we encountered EOF
        if(0 == rv) break;
        
        // asymmetric decryption call
        sodium_rv = crypto_box_seal_open(decrypted_chunk, encrypted_chunk,
                                            rv, public_key, private_key);
        if(0 != sodium_rv)
        {
            sodium_memzero(decrypted_chunk, CHUNK_SIZE);
            fputs("decryption failed, message corrupted?\n", stderr);
            goto privkeydecrypted;
        }
        
        // output decrypted data on stdout
        assert(rv >= crypto_box_SEALBYTES);
        wcnt = rv - crypto_box_SEALBYTES;
        rv = write(STDOUT_FILENO, decrypted_chunk, wcnt);
        if(rv != wcnt)
        {
            sodium_memzero(decrypted_chunk, CHUNK_SIZE);
            perror("write()");
            fputs("could not write to stdout\n", stderr);
            goto privkeydecrypted;
        }
        // clear mem (also in case something goes wrong later)
        sodium_memzero(decrypted_chunk, CHUNK_SIZE);
    }
    while(rv == CHUNK_SIZE); // if there was a full chunk, read the next one
    
    
    // --- 15 --- cleanup and finish
    
    // if we arrived at this point, return success exit status after cleanup
    success = true;
    
privkeydecrypted:
    // clear mem for private_key
    sodium_memzero(private_key, crypto_box_SECRETKEYBYTES);
    
pwhashed:
    // clear mem for symmetric_key
    sodium_memzero(symmetric_key, KEY_BYTES);
    
    if(success) return 0;
    return 1;
}
