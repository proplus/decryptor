DESCRIPTION:

This software decrypts a file encrypted by the TREES plugin for
the dovecot IMAP server and prints the decrypted content on stdout.
The TREES plugin is available here:
https://0xacab.org/riseuplabs/trees/

For decryption, the IMAP password and the crypto parameters used
during encryption need to be known.


BUILDING:

needs libsodium-dev 1.0.14 (or later)
and the standard unix libraries.

compile with:
gcc -Wall -o decryptor -lsodium decryptor.c


USAGE:

./decryptor --salt salt_hex --nonce nonce_hex --secretbox secretbox_hex --opslimit <number> --memlimit <number> [--argon2i | --argon2id] file_to_be_decrypted

'_hex' indicates a hexadecimal encoded value (no spaces or colons).
Default pwhash is argon2i if no flag is given.


LICENCE:

AGPLv3, see the file called COPYING.


Copyright (C) 2020 proplus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
